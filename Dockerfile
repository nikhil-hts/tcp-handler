FROM node:12.13.1-alpine AS BUILD_IMAGE

RUN apk update && apk add yarn curl bash python g++ make && rm -rf /var/cache/apk/*

RUN curl -sfL https://install.goreleaser.com/github.com/tj/node-prune.sh | bash -s -- -b /usr/local/bin

WORKDIR /usr/src/app


COPY package.json ./

RUN yarn --production=true

COPY . .

RUN /usr/local/bin/node-prune

FROM node:12-alpine

WORKDIR /usr/src/app

COPY --from=BUILD_IMAGE /usr/src/app/ .
COPY --from=BUILD_IMAGE /usr/src/app/node_modules ./node_modules

EXPOSE 3041
CMD ["node", "app1.js"]

