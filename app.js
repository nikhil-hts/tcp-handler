//Import Net
const net = require('net');
const tcpClient = new net.Socket();

const port = process.env.PORT || 3041;
const host = '0.0.0.0';

tcpClient
  .connect(port, host, function () {
    console.log('TCP Client Connected');
    const connectMsg =
      '010036013030303030303030303030303030303000000000000000000000000000000000410000000135383230384138303030304530303030';
    console.log(`TCP data published : ${connectMsg}`);
    const bufferRes = Buffer.from(connectMsg, 'hex');
    tcpClient.write(bufferRes);
  })
  .on('error', (err) => {
    if (err.errno === 'ECONNREFUSED') {
      console.log('Connection refused from TCP Data Handler server');
      tcpClient.on('close', function () {
        console.log('TCP Connection closed');
      });
    }
  })
  .on('data', function (data) {
    console.log('Received: ' + data.toString());
    const data1 =
      '01003b2161636237313339393030393230383533653538386565303435613965393439350011111236353832303841383030303045303030308500';
    const bufferRes = Buffer.from(data1, 'hex');
    // Send data to TCP server
    tcpClient.write(bufferRes);
    console.log(`TCP data published : ${data1}`);
  });
